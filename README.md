# tag_detector

Detector for both aruco and apriltags.


Run `roslaunch tag_detector start_detectors.launch start_april:=true`

Requires the local install of apriltag and cv2.

`pip install apriltag` should do the trick.