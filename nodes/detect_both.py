#! /usr/bin/env python3

import time

import cv2
import numpy as np
import rospy
from cv_bridge import CvBridge
from dt_apriltags import Detector
from sensor_msgs.msg import CompressedImage, Image

import apriltag


class TagDetector:
    def __init__(self) -> None:
        # ROS parameters (configurable through launch file)
        self.image_topic = rospy.get_param(
            "~image_topic", "/usb_cam/image_raw/compressed"
        )
        self.output_image_topic = rospy.get_param("~output_image_topic", "/annotations")
        self.marker_size = rospy.get_param("~marker_size", 0.127)

        self.aruco_dict_id = rospy.get_param("~aruco_dict_id", cv2.aruco.DICT_4X4_250)
        self.aruco_params = cv2.aruco.getPredefinedDictionary(self.aruco_dict_id)
        self.april_dict_id = rospy.get_param("~april_dict_id", "tag36h11")

        self.detector = Detector(
            families=self.april_dict_id,
            nthreads=4,
            quad_decimate=1.0,
            quad_sigma=0.0,
            refine_edges=1,
            decode_sharpening=0.25,
            debug=0,
        )

        # self.april_params = apriltag.DetectorOptions(
        #     families=self.april_dict_id,
        #     nthreads=4,
        #     quad_decimate=1.0,
        #     quad_blur=1.0,
        #     refine_edges=True,
        #     refine_decode=True,
        #     refine_pose=False,
        #     debug=False,
        #     quad_contours=True,
        # )

        # self.detector = apriltag.Detector(self.april_params)

        # Publishers
        self.image_pub = rospy.Publisher(self.output_image_topic, Image, queue_size=10)
        # OpenCV bridge
        self.bridge = CvBridge()

        # Subscribe to image topic
        image_sub = rospy.Subscriber(
            self.image_topic, CompressedImage, self.image_callback
        )

        # Define thickness (adjust as desired)
        self.thickness = 3

    def image_callback(self, data):
        image = self.bridge.compressed_imgmsg_to_cv2(data)
        if len(image.shape) == 3 and image.shape[2] == 3:
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            color_image = image
        else:
            gray_image = image
            color_image = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)

        output_img = color_image.copy()

        # Aruco detection
        t0 = time.time()
        corners, ids, rejected = cv2.aruco.detectMarkers(color_image, self.aruco_params)
        t1 = time.time()

        # Draw detection results on the image
        if corners is not None and ids is not None:
            rospy.loginfo(
                f"{len(ids)} total ArucoTags detected in {1000*(t1 - t0):.3f}ms"
            )

            if len(corners) > 0:
                # Draw detected markers
                # cv2.aruco.drawDetectedMarkers(output_img, corners, ids)
                for i, corner in enumerate(corners):
                    # Get corner points as integers (round for thicker lines)
                    int_corner = np.intp(corner.reshape(-1, 2))
                    # Draw filled polygon for the marker (optional)
                    # cv2.fillPoly(image, [int_corner], (0, 255, 0))  # Green filling (BGR format)

                    # Draw the thick border lines
                    for j in range(4):
                        # Get starting and ending points for each line segment
                        start_point = int_corner[j]
                        end_point = int_corner[(j + 1) % 4]

                        # Draw the line segment with specified thickness
                        cv2.line(
                            output_img,
                            start_point,
                            end_point,
                            (255, 0, 0),
                            self.thickness,
                        )
        else:
            rospy.loginfo_throttle(period=1, msg="No ArucoTags detected")

        t0 = time.time()
        results = self.detector.detect(gray_image)
        t1 = time.time()
        if len(results) > 0:
            rospy.loginfo(
                f"{len(results)} total AprilTags detected in {1000*(t1 - t0):.3f}ms"
            )

            for tag in results:
                for idx in range(len(tag.corners)):
                    cv2.line(
                        output_img,
                        tuple(tag.corners[idx - 1, :].astype(int)),
                        tuple(tag.corners[idx, :].astype(int)),
                        (0, 255, 0),
                        thickness=self.thickness,
                    )

                    cv2.putText(
                        output_img,
                        str(tag.tag_id),
                        org=(
                            tag.corners[0, 0].astype(int) + 10,
                            tag.corners[0, 1].astype(int) + 10,
                        ),
                        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                        fontScale=0.5,
                        color=(0, 255, 0),
                        thickness=2,
                    )

        else:
            rospy.loginfo_throttle(period=1, msg="No AprilTags detected")

        # Convert OpenCV image back to ROS image and publish
        ros_image = self.bridge.cv2_to_imgmsg(output_img, encoding="bgr8")
        self.image_pub.publish(ros_image)


if __name__ == "__main__":
    rospy.init_node("tag_detection_node")
    try:
        detector = TagDetector()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
